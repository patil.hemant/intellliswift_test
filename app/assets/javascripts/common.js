function characterLeftCounter(){
	var $charcounter = $("<div class='char-counter'>characters left: <span id='counter'></span></div>")
    $("input,textarea").unbind('keyup').bind("keyup",function(){
      var maxlength= parseInt($(this).attr("maxlength"))
      if(!isNaN(maxlength)){
        $charcounter.find("#counter").text(maxlength - $(this).val().length)
        $(this).closest(".form-group").append($charcounter)
      }
    })
}