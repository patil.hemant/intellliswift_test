class EmployeeDatatable
	delegate :params, :link_to, :raw, :rescue, :truncate, :number_to_currency, to: :@view
	
	def initialize(view)
		@view = view
	end

	def as_json(options = {})
    {
        sEcho: params[:sEcho].to_i,
        iTotalRecords: Employee.all.count,
        iTotalDisplayRecords: employees.total_entries,
        aaData: data
    }
 	end


	private

	def data
		employees.order('created_at desc').map do |employee|
			[
			  	employee.name,
			  	employee.email,
			  	employee.emp_id,
			  	employee.skills.pluck(:skill).join(', '),
			]
		end
	end  

	def employees
		@employees = fetch_employees
	end
 	
 	def fetch_employees
 		employees = Employee.order("#{sort_column} #{sort_direction}").includes(:skills)
 		employees = employees.page(page).per_page(per_page)
 		if params[:sSearch].present?
	      employees = employees.where(" name like :search or email like :search or emp_id like :search", search: "%#{params[:sSearch]}%").includes(:skills)
	    end
    	return employees
 	end

 	def page
 		params[:iDisplayStart].to_i / per_page.to_i + 1
 	end

 	def per_page
 		params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
 	end

 	def sort_column
 		columns = ["action","name","created_at","updated_at"]
 		columns[params[:iSortCol_0].to_i]
 	end

 	def sort_direction
 		params[:sSortDir_0] == 'desc' ? ' desc ' : 'asc'
 	end
end