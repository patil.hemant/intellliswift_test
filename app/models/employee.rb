class Employee < ApplicationRecord
	has_many :skills, :foreign_key => "emp_id", :dependent => :destroy
end
