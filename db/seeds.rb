# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)



skills1 = ['c++', 'Ruby','Rails','Node', 'HTML']
skills2 = ['Drawing', 'Singing','Photoshop']
10000.times do |i|
	emp = Employee.create(:name => (0...5).map { ('a'..'z').to_a[rand(10)] }.join, :email => (0...5).map { ('a'..'z').to_a[rand(10)] }.join + "@" + "gmail.com", :emp_id => i)
	if 	i % 2 == 0
		skills1.each_with_index do |skill, index|		
			Skill.create(:emp_id => emp.id, :skill => skill, :experience => index + 1)
		end
	else
		skills2.each_with_index do |skill, index|		
			Skill.create(:emp_id => emp.id, :skill => skill, :experience => index + 1)
		end
	end
end

