class CreateSkills < ActiveRecord::Migration[5.0]
  def change
    create_table :skills do |t|
      t.integer :emp_id
      t.integer :experience
      t.string :skill

      t.timestamps
    end
    add_index :skills, :emp_id
  end
end
